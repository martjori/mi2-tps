const UTILS = {

    getJson: async function (fileURI) {
        const headers = new Headers();
        headers.append('Content-type', 'application/json')
        headers.append('Accept', 'application/json');

        try {
            const res = await fetch(window.location.origin + fileURI, {
                method: 'GET',
                headers: headers
            });

            return await res.json();
        } catch (e) {
            alert(e);
        }
    },

    getHTML: async function(htmlPage) {
        try {
            const res = await fetch(window.location.origin + '/' + htmlPage, {
                method: 'GET'
            });

            return await res.text();
        } catch (e) {
            alert(e);
        }
    }
}

const MENU = {

    active: '',

    addCaseMenu: (elem, parent) => {
        const keys = Object.keys(elem);

        let haveSubMenu = false;

        if (keys.includes('connected') && elem['connected'] === true) {
            return;
        } else if (keys.includes('subMenu')) {
            haveSubMenu = true;
        }

        const li = document.createElement('li');
        parent.appendChild(li);

        const a = document.createElement('a');
        a.innerText = elem['label'];
        a.id = elem['label'];

        if (elem['link']) {
            a.addEventListener('click', () => {
                const activeElem = document.getElementById(MENU.active);
                if (activeElem) activeElem.className = '';

                a.classList.add('active');
                MENU.active = a.id;
                MENU.changeCurrentPage(elem['link']);
            });
        }

        li.appendChild(a);

        if (haveSubMenu) {
            li.classList.add('section');
            const subMenu = document.createElement('ul');
            subMenu.classList.add('sub_menu');
            li.appendChild(subMenu)

            for (const sub of elem['subMenu']) {
                MENU.addCaseMenu(sub, subMenu);
            }
        }
    },

    generateMenu: () => {

        const header = document.createElement('header');
        header.innerHTML = '<h1>Club de Foot de Meylan</h1>';

        const nav = document.createElement('nav');
        nav.id = 'menu';

        header.appendChild(nav);

        const ul = document.createElement('ul');
        nav.appendChild(ul);

        UTILS.getJson('/data/menu.json').then(menu => {

            for (const elem of menu) {
                MENU.addCaseMenu(elem, ul);
            }

        });

        document.body.appendChild(header);
        document.body.appendChild(document.createElement('hr'));
    },

    changeCurrentPage: (newPage) => {
        currentPage = newPage;
        actualisePage();
    }
}


let root;
let currentPage;

const initialisePage = () => {
    MENU.generateMenu();

    root = document.createElement('div');
    root.id = 'root';

    document.body.appendChild(root);

    currentPage = 'pages/main.html';

    actualisePage();
}

const actualisePage = () => {
    root.innerHTML = '';

    UTILS.getHTML(currentPage).then(html => {
        root.innerHTML = html;

        if (document.getElementById('joueur_table')) {
            JOUEURS.initPlayers();
        }
    });
}

initialisePage();