const JOUEURS = {
    players: [],
    editingPlayer: null,
    form: {},

    addTableRow: (playerData) => {
        const tr = document.createElement('tr');
        tr.classList.add('added_row');

        for (const key in playerData) {

            if (key === 'id') continue;

            const td = document.createElement('td');

            switch (key) {

                case 'image':
                    const img = document.createElement('img');
                    img.src = playerData[key];
                    img.alt = "Photo de " + playerData.name;
                    td.appendChild(img);
                    break;

                case 'description':
                    const ul = document.createElement('ul');

                    const ageLi = document.createElement('li');
                    ageLi.innerText = 'Age : ' + playerData[key].age;
                    ul.appendChild(ageLi);

                    const oldClubsLi = document.createElement('li');
                    oldClubsLi.innerText = 'Anciens clubs : ' + playerData[key].oldClubs.join(', ');
                    ul.appendChild(oldClubsLi);

                    td.appendChild(ul);
                    break;

                default:
                    td.innerText = playerData[key];
            }
            tr.appendChild(td);

        }

        const tdButton = document.createElement('td');
        tr.appendChild(tdButton);

        const editButton = document.createElement('button');
        editButton.innerText = '✏️';
        editButton.addEventListener('click', () => {
            JOUEURS.editingPlayer = playerData;
            JOUEURS.updateForm(playerData);
        });
        tdButton.appendChild(editButton);

        const deleteButton = document.createElement('button');
        deleteButton.innerText = '❌';
        deleteButton.addEventListener('click', () => {
            JOUEURS.deletePlayer(playerData.id);
        });
        tdButton.appendChild(deleteButton);

        const tbody = document.querySelector('tbody');
        tbody.insertBefore(tr, tbody.lastElementChild);
    },

    clearTable: () => {
        Array.from(document.getElementsByClassName('added_row')).forEach(element => {
            element.remove();
        });
    },

    generatePlayerTable: () => {

        JOUEURS.clearTable();

        for (const player of JOUEURS.players) {
            JOUEURS.addTableRow(player);
        }
    },

    addPlayer: (playerData) => {

        JOUEURS.players.push(playerData);

        JOUEURS.generatePlayerTable();
    },

    editPlayer: (playerData) => {
        const index = JOUEURS.players.findIndex(elem => elem.id === playerData.id);
        JOUEURS.players.splice(index, 1, playerData);
        JOUEURS.generatePlayerTable();
    },

    deletePlayer: (playerId) => {
        const index = JOUEURS.players.findIndex(elem => elem.id === playerId);
        JOUEURS.players.splice(index, 1);
        JOUEURS.generatePlayerTable();
    },

    updateForm: (playerData) => {

        const form = JOUEURS.form;

        form.name.value = playerData.name;
        form.position.value = playerData.position;
        form.age.value = playerData.description.age;
        form.oldClubs.value = playerData.description.oldClubs.join(',');
        form.image.src = playerData.image;

        form.container.style.visibility = 'visible';

    },

    clearForm: () => {
        const form = JOUEURS.form;

        form.name.value = '';
        form.position.value = '';
        form.age.value = '';
        form.oldClubs.value = '';
        form.image.src = '';
    },

    initPlayers: () => {

        JOUEURS.form = {
            container: document.getElementById('add_row_container'),
            name: document.getElementById('player_name'),
            position: document.getElementById('player_position'),
            age: document.getElementById('player_age'),
            oldClubs: document.getElementById('player_old_clubs'),
            file: document.getElementById('player_image_file'),
            image: document.getElementById('player_image')
        },

        UTILS.getJson('/data/joueurs.json').then(players => {
            JOUEURS.players = [];
            players.forEach((pl, index) => {
                pl['id'] = 'player_' + index;
                JOUEURS.players.push(pl);
            });

            JOUEURS.generatePlayerTable();
        });


        document.getElementById('add_row_button').addEventListener('click', () => {
            JOUEURS.form.container.style.visibility = 'visible';
        });

        document.getElementById('add_row_exit').addEventListener('click', () => {
            JOUEURS.form.container.style.visibility = 'hidden';
            JOUEURS.clearForm();
        }); 

        const fileInput = document.getElementById('player_image_file');
        fileInput.addEventListener('change', (event) => {
            const file = event.target.files[0];

            const img = JOUEURS.form.image;
            img.width = '100px';
            img.src = URL.createObjectURL(file);
            
        });

        const form = document.getElementById('add_row_form');

        form.addEventListener('submit', (event) =>  {
            event.preventDefault();

            const form = JOUEURS.form;

            const playerData = {
                image: form.image.src,
                name: form.name.value,
                position: form.position.value,
                description: {
                    age: form.age.value,
                    oldClubs: form.oldClubs.value.split(',').map(elem => elem.trim())
                },
                id: JOUEURS.editingPlayer ? JOUEURS.editingPlayer.id : 'player_' + Date.now()
            }

            if (JOUEURS.editingPlayer) JOUEURS.editPlayer(playerData);
            else JOUEURS.addPlayer(playerData);

            JOUEURS.clearForm();

            JOUEURS.form.container.style.visibility = 'hidden';
        });
    }
}